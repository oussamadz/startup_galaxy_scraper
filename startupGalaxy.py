#!/usr/bin/python3

THREAD_COUNT = 8
FILE_NAME = 'startupGalaxy.csv'


from bs4 import BeautifulSoup as bs
import requests as rq
import threading 
import queue
import os 
import datetime 
import tools




if not os.path.exists(FILE_NAME):
    with open(FILE_NAME, 'a') as f:
        f.write('name,location,summary,website,linkedin\n')

def work(n, q):
    while not q.empty():
        locking.acquire()
        url = q.get()
        locking.release()
        pga = rq.get(url)
        print(f"{n} Started: ")
        ps2 = bs(pga.text, 'html.parser')
        name = ps2.find('h1', class_='card-title').text.replace('\n','').replace(',','')
        summary = ps2.find('p', class_='card-text').text.replace('\n','').replace(',','').replace('\r','').replace('\s','')
        boxes = ps2.find_all('div', class_='card module small-module Normal')
        location = 'N/A'
        links = []
        for box in boxes:
            if 'Location' in box.find('div', class_='card-header').text:
                location = box.find('div', class_='card-body').text.replace('\n','').replace(',','')
            if 'Links & Contact' in box.find('div', class_='card-header').text:
                links = box.find_all('tr')
        trs = links
        website = "N/A"
        linkedin = "N/A"
        for tr in trs:
            if 'Website' in tr.text:
                website = tr.findNext('tr').text.replace('\n','').replace(',','')
            if 'LinkedIn' in tr.text:
                linkedin = tr.findNext('tr').find('a').get('href')
        locking.acquire()
        print(f"name     : {name}")
        print(f"location : {location}")
        print(f"website  : {website}")
        print(f"linkedin : {linkedin}")
        print(f"summary  : {summary} ....")
        print("____________________________________________")
        line = f"{name}, {location}, {summary}, {website}, {linkedin}" + "\n"
        #line = summary.rstrip()
        with open(FILE_NAME, 'a') as fr:
            fr.write(line)
        locking.release()




page = rq.get("https://startupgalaxy.com.au/browse?types=Startup")

ps = bs(page.text, 'html.parser')
container = ps.find('div', id="browse-result")
items = container.find_all('a', class_='tile-link')

qu = queue.Queue()
locking = threading.Lock()

for it in items:
    qu.put('https://startupgalaxy.com.au' + it.get('href'))
threads = []





for i in range(THREAD_COUNT):
    nm = f"Thread_{i}"
    t = threading.Thread(target=work, name=nm, args=(nm,qu))
    t.start()
    threads.append(t)
for t in threads:
    t.join()

print(f"All done script terminated at : {datetime.datetime.now()}")
