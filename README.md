# startup_galaxy_scraper



## Introduction

This script was specifically tailored for scraping startupGalaxy website using requests, BeautifuSoup, multithreading, and a special developed multipurpose tool. 

## Usage

First things first, make sure your pip has the below packages installed:
```
requests
bs4
```

To run the script simply run the command:

```
python3 startupGalaxy.py
```
And the script will run using [8 threads] to scrape and save all data in csv file named [startupGalaxy.csv].
To change those two parameters, simply modify line 3 and 4 in script. 
