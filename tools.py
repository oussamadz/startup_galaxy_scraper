'''
This Class was made by Oussama Mechri for ease to use and faster script writing
please do not edit it or publish it for any reasons
'''
import os
import pandas as pd
import xlsxwriter
import datetime
from selenium import webdriver
class tool(object):
	url = ""
	ids = []
	
	begine = 0
	ended = 0
	def start_time(self):
		global begine
		begine = time.time()
		return 'Started at : {0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())

	def end_time(self):
		global ended
		ended = time.time()
		return 'Ended at : {0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
	
	def profile(self, Image, flash, javascript, css):
		firefox_profile = webdriver.FirefoxProfile()
		firefox_profile.set_preference("general.useragent.override", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
		if Image:
			firefox_profile.set_preference('permissions.default.image', 2)
		if flash:
			firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
		if javascript:
			firefox_profile.set_preference('javascript.enabled', False)
		if css:
			firefox_profile.set_preference('permissions.default.stylesheet', 2)
		return firefox_profile
	
	def save_progress(self,age_count):
		if os.path.exists('progress'):
			with open('progress','w') as fn:
				fn.write(str(page_count))
		else:
			fn = open('progress', 'w')
			fn.write(page_count)

	def write_csv(self,line,file_name):
		if os.path.exists(file_name):
			with open(file_name ,'a',encoding='utf-8') as file:
				file.write(line)
		else:
			file = open(file_name,'a', encoding='utf-8')
			file.write(line)

	def write_xlsx(self,pd_table,filename):
		writer = pd.ExcelWriter(filename, engine='xlsxwriter')
		pd_table.to_excel(writer,sheet_name='Sheet1')
		writer.save()
		writer.close()

	def clean_text(self,text):
		return text.replace('\n', '').replace(',', '').replace('\t','').replace('\xa0','').replace('\r','').replace('\s','')
	
	def add_id(self,id):
		global ids
		ids.append(id)

	def check_duplicate(self,items_number):
		ball = False
		global ids
		for i in ids:
			if items_number == i:
				ball = True
		return ball

	def progress(self,page, full):
		p = page*100/full
		print('Progress : [' + '{:.2f}'.format(p) + "%]")
		bar = "[" 
		s = 1
		while s <= 50:
			if s <= int(p/2):
				bar+='█'
			else:
				bar+='_'
			s+=1
		bar +=']'
		print(bar)
			
